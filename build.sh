#!/bin/sh -x
set -e
echo "Deploy started on $(date)"

PRERELEASE_SUFFIX=""
PRERELEASE_SUFFIX_CLASSIFIER=""
PRERELEASE_SUFFIX_REV=""
BASEREV=$(git -C "$1" describe --tags --abbrev=0)
if [ -z "$(git -C "$1" tag -l --points-at HEAD | grep -v '^_')" ]; then
    PRERELEASE_SUFFIX_CLASSIFIER="-beta"
    PRERELEASE_SUFFIX_REV="$(git -C "$1" log -1 --format="%h")"
    PRERELEASE_SUFFIX="$PRERELEASE_SUFFIX_CLASSIFIER+$PRERELEASE_SUFFIX_REV"
    BASEREV=$(git -C "$1" describe --tags --abbrev=0 | python -c 'import semver, sys;print(semver.bump_patch(sys.stdin.read()))')
fi

POINTZI_VERSION="${BASEREV}$PRERELEASE_SUFFIX"

sed -e "s:{{VERSION}}:$POINTZI_VERSION:g" \
    paper-onboarding-pointzi.podspec.in > paper-onboarding-pointzi.podspec

pod spec lint "paper-onboarding-pointzi.podspec" --allow-warnings --verbose

if [ "$PRERELEASE_SUFFIX" == "" ]; then
  echo "Deploy to cocoapod"
  # always deploy to trunk see https://github.com/CocoaPods/CocoaPods/issues/7086
  pod trunk push pointzi.podspec --allow-warnings
fi
